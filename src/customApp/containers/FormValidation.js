import React, { Component } from 'react';
import { Input } from 'antd';
import Form from '../../components/uielements/form';
import Notification from '../../components/notification';
const FormItem = Form.Item;

class FormWIthSubmissionButton extends Component {
  state = {
    confirmDirty: false,
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        Notification(
          'success',
          'Received values of form',
          JSON.stringify(values)
        );
      }
    });
  };

  checkBooking = (str, value, callback) => {
    const form = this.props.form;
    const specialChar = /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/g
    if ((value.match(/^[A-Z0-9]+$/g)) !== null){
      callback();
    } else {
      callback('Should be a combination of numbers and capitalized alphabets!');
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    };
    return (
      <Form onSubmit={this.handleSubmit}>
      
        <FormItem {...formItemLayout} hasFeedback>
          {getFieldDecorator('email', {
            rules: [
              {
                required: true,
                message: 'Please input your booking code!',
              },
              {
                validator: this.checkBooking,
              },
            ],
          })(<Input name="booking code" id="booking code" />)}
        </FormItem>
      </Form>
    );
  }
}

const WrappedFormWIthSubmissionButton = Form.create()(FormWIthSubmissionButton);
export default WrappedFormWIthSubmissionButton;
